require 'rails_helper'

RSpec.describe Spree::QuoteDetails, type: :model do
  context 'validations' do
    [:quote].each do |attr|
      it { is_expected.to validate_presence_of(attr) }
    end
  end

  context "associations" do
    it { is_expected.to belong_to(:quote) }
  end
end
