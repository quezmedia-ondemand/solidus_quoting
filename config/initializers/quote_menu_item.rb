# quote menu_item
quote_menu_item = Spree::BackendConfiguration::MenuItem.new(
  [:quotes],
  'signal'
)

Spree::Backend::Config.menu_items << quote_menu_item
