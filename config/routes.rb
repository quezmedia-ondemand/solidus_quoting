Spree::Core::Engine.routes.draw do
  patch '/save_quote_cart', to: 'orders#save_as_quote_cart', as: :save_as_quote_cart

  namespace :admin do
    resources :quotes, except: [:show] do
      member do
        get :cart
        get "/adjustments/unfinalize", to: "quotes#unfinalize_adjustments"
        get "/adjustments/finalize", to: "quotes#finalize_adjustments"
        post :send_to_user
        post :save_as_order
        get :quote_request
      end

      resources :adjustments, controller: "quotes/adjustments"
      resource :customer, controller: "quotes/customer_details"

      collection do
        resources 'quote_requests', controller: 'quotes/requests', only: [:index, :show] do
          member do
            get 'create_quote', to: 'quotes/requests#create_quote'
          end
        end
      end
    end
  end

  resources :quotes do
    member do
      get :approve
      get :place_into_cart
      get :leave_it
    end
  end

  resources :quote_requests, only: [:new, :create, :show] do
    post :comment, to: 'quote_requests#create_comment', on: :collection
  end
end
