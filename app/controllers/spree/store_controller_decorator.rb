module Spree
  if defined? Spree::Frontend
    StoreController.class_eval do
      alias_method :old_cart_link, :cart_link

      def cart_link
        if session.has_key?(:current_quote_number)
          @simple_current_order = Spree::Order.find_by_number(session[:current_quote_number])
        end

        old_cart_link
        if @simple_current_order.completed?
          @order = @current_order = @simple_current_order = nil
          if session.has_key?(:current_quote_number)
            session.delete(:current_quote_number)
          end
        end
      end
    end
  end
end
