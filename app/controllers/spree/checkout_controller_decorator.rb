module Spree
  CheckoutController.class_eval do
    prepend_before_action :set_current_quote

    alias_method :old_check_authorization, :check_authorization

    def check_authorization
      return if session.has_key?(:current_quote_number) && @order.email.present?
      old_check_authorization
    end

    private
    def set_current_quote
      if session.has_key?(:current_quote_number)
        @order = @current_order = @simple_current_order = Spree::Order
          .find_by_number(session[:current_quote_number])
      end
    end
  end
end
