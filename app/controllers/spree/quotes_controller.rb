module Spree
  if defined? Spree::Frontend
    class QuotesController < Spree::StoreController
      respond_to :html
      before_action :load_quote, only: [:show, :edit, :destroy, :update, :approve, :place_into_cart]
      before_action :check_request_owner

      def show
      end

      def destroy
        if @quote.update_attributes(is_quote: false)
          redirect_to account_path
        end
      end

      def approve
        @quote.approve!

        flash[:success] = Spree.t(:approval_requested)

        respond_with(@quote) do |format|
          format.html { redirect_to quote_request_path(@quote.quote_request) }
        end
      end

      def place_into_cart
        @quote.update!
        session[:current_quote_number] = @quote.number

        redirect_to spree.checkout_state_path(@quote.state)
      end

      # Detach quote from cart
      def leave_it
        session.delete(:current_quote_number) if session.has_key?(:current_quote_number)
        @order = @simple_current_order = @current_order = nil

        redirect_to spree.root_path
      end

      private
      def load_quote
        @quote = Spree::Quote.unscoped.find_by_number!(params[:id])
      end

      def check_authorization
        @user ||= spree_current_user
        authorize! params[:action].to_sym, @user
      end

      def check_request_owner
        if try_spree_current_user.blank? || try_spree_current_user.try(:admin?) || try_spree_current_user.try(:email) == @quote.email
          return
        else
          raise CanCan::AccessDenied
        end
      end
    end
  end
end
