module Spree
  class QuoteRequestsController < Spree::StoreController
    respond_to :html

    before_action :load_quote_request, only: [:show]

    def new
      @quote_request = Spree::QuoteRequest.new(quote_request_new_params)
      @quote_request.comments.build(quote_request_comments_new_params)
    end

    def show
      @comment = Spree::QuoteComment.new(by_admin: false, commentable: @quote_request)
    end

    def create
      @quote_request = Spree::QuoteRequest.new(quote_request_params)
      @quote_request.comments.first.guest_email = @quote_request.guest_email

      if @quote_request.save
        @quote_request.deliver_quote_request_notification
        flash[:success] = Spree::Customizations.first.try(:quote_req_success_msg) || Spree.t(:quote_request_made)

        respond_with(@quote_request) do |format|
          format.html { redirect_to( quote_request_path(@quote_request) ) }
        end
      else
        render :new
      end
    end

    def create_comment
      @comment = Spree::QuoteComment.new(comment_params)

      if @comment.save
        respond_with(@comment) do |format|
          format.html do
            @comment.commentable.deliver_comment_at_quote_request_notification
            if @comment.commenter_role == 'Admin'
              if @comment.commentable.quote.blank?
                redirect_to(admin_quote_request_path(@comment.commentable))
              else
                redirect_to(quote_request_admin_quote_path(@comment.commentable.quote))
              end
            else
              redirect_to(quote_request_path(@comment.commentable))
            end
          end
        end
      else
        @quote_request = @comment.commentable
        @quote = @comment.commentable.quote
        render(@comment.by_admin == true ? 'spree/admin/quotes/quote_request' : :show)
      end
    end

    private
    def quote_request_new_params
      {
        user_id: try_spree_current_user.try(:id)
      }.with_indifferent_access
    end

    def quote_request_comments_new_params
      {
        user_id: try_spree_current_user.try(:id),
        commentable_type: Spree::QuoteRequest,
        by_admin: false
      }.with_indifferent_access
    end

    def quote_request_params
      params.require(:quote_request)
        .permit(:user_id, :guest_email, comments_attributes:
          [:id, :_destroy, :body, :user_id, :guest_email, :by_admin, :commenter_role])
    end

    def comment_params
      params.require(:quote_comment).permit(:body, :user_id, :guest_email, :by_admin,
        :commenter_role, :commentable_type, :commentable_id)
    end

    def load_quote_request
      @quote_request = Spree::QuoteRequest.find(params[:id])
      if (temp_user = try_spree_current_user) && @quote_request.user.blank? && temp_user.try(:email) == @quote_request.guest_email
        @quote_request.check_or_update_owner(try_spree_current_user)
      end
    end
  end
end
