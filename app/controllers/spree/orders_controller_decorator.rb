module Spree
  if defined? Spree::Frontend
    OrdersController.class_eval do
      alias_method :old_edit, :edit
      alias_method :old_empty, :empty
      alias_method :old_current_order, :current_order
      alias_method :old_set_current_order, :set_current_order
      alias_method :old_check_authorization, :check_authorization

      def current_order(options = {})
        set_current_quote if session.has_key?(:current_quote_number)
        old_current_order(options)
      end

      def set_current_order
        set_current_quote if session.has_key?(:current_quote_number)
        old_set_current_order
      end

      def edit
        if session.has_key?(:current_quote_number)
          set_current_quote
          session.delete(:current_quote_number) if @order.completed?
        end
        old_edit
      end

      def empty
        session.delete(:current_quote_number)
        @order = @simple_current_order = @current_order = nil
        old_empty
      end

      def check_authorization
        if session.has_key?(:current_quote_number)
          return
        else
          old_check_authorization
        end
      end

      private
      def set_current_quote
        @order = @current_order = @simple_current_order = Spree::Order
          .find_by_number(session[:current_quote_number])
      end

      def clear_current_quote
        @order = @current_order = @simple_current_order = nil
      end
    end
  end
end
