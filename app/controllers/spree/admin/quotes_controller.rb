module Spree
  if defined? Spree::Backend
    module Admin
      class QuotesController < Spree::Admin::BaseController
        before_action :load_quote, only: [:edit, :update, :unfinalize_adjustments, :finalize_adjustments, :cart, :send_to_user, :save_as_order, :destroy, :quote_request]
        around_action :lock_quote, only: [:update]

        rescue_from Spree::Quote::InsufficientStock, with: :insufficient_stock_error

        respond_to :html

        def index
          query_present = params[:q]
          params[:q] ||= {}
          params[:q][:s] ||= 'created_at desc'
          params[:q][:completed_at_not_null] = ''

          if params[:q][:created_at_gt].present?
            params[:q][:created_at_gt] = begin
                                           Time.zone.parse(params[:q][:created_at_gt]).beginning_of_day
                                         rescue
                                           ""
                                         end
          end

          if params[:q][:created_at_lt].present?
            params[:q][:created_at_lt] = begin
                                           Time.zone.parse(params[:q][:created_at_lt]).end_of_day
                                         rescue
                                           ""
                                         end
          end

          @search = Spree::Quote.accessible_by(current_ability, :index).ransack(params[:q])

          # lazy loading other models here (via includes) may result in an invalid query
          # e.g. SELECT  DISTINCT DISTINCT "spree_orders".id, "spree_orders"."created_at" AS alias_0 FROM "spree_orders"
          # see https://github.com/spree/spree/pull/3919
          @quotes = if query_present
            @search.result(distinct: true)
          else
            @search.result
          end

          @quotes = @quotes.
            page(params[:page]).
            per(params[:per_page] || Spree::Config[:orders_per_page])
        end

        def new
          user = Spree.user_class.find_by_id(params[:user_id]) if params[:user_id]
          @quote = Spree::Core::Importer::Order.import(user, quote_params)
          redirect_to cart_admin_quote_url(@quote)
        end

        def edit
          require_ship_address

          unless @quote.completed?
            @quote.refresh_shipment_rates
          end
        end

        def cart
          unless @quote.completed?
            @quote.refresh_shipment_rates
          end
          if @quote.shipped_shipments.count > 0
            redirect_to edit_admin_quote_url(@quote)
          end
        end

        def quote_request
          @comment = Spree::QuoteComment.new(by_admin: true, commentable: (@quote_request = @quote.quote_request))
        end

        def unfinalize_adjustments
          adjustments = @quote.all_adjustments.finalized
          adjustments.each(&:unfinalize!)
          flash[:success] = Spree.t(:all_adjustments_unfinalized)

          respond_with(@quote) { |format| format.html { redirect_to(spree.admin_quote_adjustments_path(@quote)) } }
        end

        def finalize_adjustments
          adjustments = @quote.all_adjustments.not_finalized
          adjustments.each(&:finalize!)
          flash[:success] = Spree.t(:all_adjustments_finalized)

          respond_with(@quote) { |format| format.html { redirect_to(spree.admin_quote_adjustments_path(@quote)) } }
        end

        def send_to_user
          @quote.send_quote_to_user
          flash[:success] = Spree.t(:quote_sent_to_user)

          redirect_to(spree.edit_admin_quote_path(@quote))
        end

        def save_as_order
          @quote.finish_quote

          flash[:success] = Spree.t(:saved_as_order)

          redirect_to(spree.edit_admin_order_path(@quote))
        end

        def destroy
          @quote.destroy

          flash[:success] = Spree.t(:discarded_quote)

          respond_with(@quote) do |format|
            format.html { redirect_to(spree.admin_quotes_path) }
            format.js { render partial: 'spree/admin/quotes/destroy' }
          end
        end

        private

        def quote_params
          {
            created_by_id: try_spree_current_user.try(:id),
            frontend_viewable: false,
            store_id: current_store.try(:id),
            is_quote: true,
            quote_state: 'created'
          }.with_indifferent_access
        end

        def load_quote
          @quote = Spree::Quote.includes(:adjustments).find_by_number!(params[:id])
          authorize! action, @quote
        end

        def model_class
          Spree::Quote
        end

        def insufficient_stock_error
          flash[:error] = Spree.t(:insufficient_stock_for_quote)
          redirect_to cart_admin_quote_url(@quote)
        end

        def require_ship_address
          if @quote.ship_address.nil?
            flash[:notice] = Spree.t(:fill_in_customer_info)
            redirect_to edit_admin_quote_customer_url(@quote)
          end
        end
      end
    end
  end
end
