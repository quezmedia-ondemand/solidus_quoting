module Spree
  if defined? Backend
    module Admin
      module Quotes
        class RequestsController < Spree::Admin::BaseController
          before_action :load_quote_request, only: [:show, :create_quote]

          def index
            query_present = params[:q]

            @search = Spree::QuoteRequest.accessible_by(current_ability, :index).ransack(params[:q])
            @quote_requests = if query_present
              @search.result(distinct: true)
            else
              @search.result
            end

            @quote_requests = @quote_requests.page(params[:page]).
              per(params[:per_page] || Spree::Config[:orders_per_page])
          end

          def show
            @comment = Spree::QuoteComment.new(by_admin: true, commentable: @quote_request)
          end

          def create_quote
            user = Spree.user_class.find_by_id(params[:user_id]) if params[:user_id]
            @quote = Spree::Core::Importer::Order.import(user, quote_params)
            @quote_request.update_columns(spree_order_id: @quote.id, status: Spree::QuoteRequest.statuses[:approved])
            redirect_to quote_request_admin_quote_url(@quote)
          end

          private
          def load_quote_request
            @quote_request = Spree::QuoteRequest.find(params[:id])
          end

          def quote_params
            {
              created_by_id: try_spree_current_user.try(:id),
              frontend_viewable: false,
              store_id: current_store.try(:id),
              is_quote: true,
              quote_state: 'created'
            }.with_indifferent_access
          end
        end
      end
    end
  end
end
