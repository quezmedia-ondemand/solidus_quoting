module Spree
  if defined? Backend
    module Admin
      module Quotes
        class CustomerDetailsController < Spree::Admin::BaseController
          rescue_from Spree::Quote::InsufficientStock, with: :insufficient_stock_error

          before_action :load_quote

          def show
            edit
          end

          def edit
            country_id = Spree::Country.default.id
            @quote.build_bill_address(country_id: country_id) if @quote.bill_address.nil?
            @quote.build_ship_address(country_id: country_id) if @quote.ship_address.nil?

            @quote.bill_address.country_id = country_id if @quote.bill_address.country.nil?
            @quote.ship_address.country_id = country_id if @quote.ship_address.country.nil?
          end

          def update
            if @quote.contents.update_cart(quote_params)

              if should_associate_user?
                requested_user = Spree.user_class.find(params[:user_id])
                @quote.associate_user!(requested_user, @quote.email.blank?)
              end

              unless @quote.quote_request
                create_quote_request
                @quote.send_quote_request_notification
              end

              unless @quote.completed?
                @quote.next
                @quote.refresh_shipment_rates
              end

              flash[:success] = Spree.t('customer_details_updated')
              redirect_to cart_admin_quote_url(@quote)
            else
              render action: :edit
            end
          end

          private

          def quote_params
            params.require(:order).permit(
              :email,
              :use_billing,
              :company,
              :sales_rep_id,
              :doc_hours_from,
              :doc_hours_to,
              bill_address_attributes: permitted_address_attributes,
              ship_address_attributes: permitted_address_attributes,
            )
          end

          def load_quote
            @quote = Spree::Quote.includes(:adjustments).find_by_number!(params[:quote_id])
          end

          def model_class
            Spree::Quote
          end

          def should_associate_user?
            params[:guest_checkout] == "false" && params[:user_id] && params[:user_id].to_i != @quote.user_id
          end

          def insufficient_stock_error
            flash[:error] = Spree.t(:insufficient_stock_for_order)
            redirect_to edit_admin_quote_customer_url(@quote)
          end

          def create_quote_request
            @qr ||= @quote.create_quote_request(
              user_id: @quote.user_id,
              status: Spree::QuoteRequest.statuses[:approved]
            )
            @qr.guest_email = @quote.email if @qr.user_id.blank?
          end
        end
      end
    end
  end
end
