class Spree::QuoteDetails < ActiveRecord::Base
  # Validations
  validates :quote, presence: true

  # Associations
  belongs_to :quote, class_name: Spree::Order, foreign_key: :spree_order_id

  # Delegations
  delegate :user, to: :quote, prefix: true

  def processing_quote?
    %w(requested revision).include?(quote_state)
  end

  def approved_by_user?
    approved?
  end

  def needs_revision?
    quote_changed?
  end

  def finished?
    quote.quote_closed?
  end
end
