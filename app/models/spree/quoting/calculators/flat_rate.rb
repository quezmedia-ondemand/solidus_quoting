module Spree
  module Quoting
    module Calculators
      class FlatRate < Spree::Calculator
        preference :amount, :decimal, default: 0
        preference :currency, :string, default: ->{ Spree::Config[:currency] }

        def self.description
          "Flat Rate Calculator For Quoting"
        end

        def compute(object = nil)
          # TODO
          if object && preferred_currency.casecmp(object.currency).zero?
            preferred_amount
          else
            0
          end
        end

        def available?(object)
          object.is_quote?
        end
      end
    end
  end
end
