Spree.user_class.class_eval do
  after_create :associate_quote_requests

  has_many :quotes,             ->() { as_quote },
                                foreign_key: "user_id",
                                class_name: "Spree::Quote"

  has_many :quote_orders,    ->() { where(quote_closed: true, is_quote: false, completed_at: nil) },
                                foreign_key: "user_id",
                                class_name: "Spree::Quote"

  has_many :unfinished_quotes,  ->() { where(is_quote: true, quote_closed: false, completed_at: nil) },
                                foreign_key: "user_id",
                                class_name: "Spree::Quote"


  has_many :quote_requests,     foreign_key: 'user_id',
                                class_name: "Spree::QuoteRequest"

  has_many :created_quotes,     class_name: "Spree::Quote",
                                foreign_key: "sales_rep_id",
                                dependent: :nullify

  # overrides
  alias_method :old_last_incomplete_spree_order, :last_incomplete_spree_order
  # @return [Spree::Order] the most-recently-created incomplete order that is not a quote
  # since the customer's last complete order.
  def last_incomplete_spree_order(store: nil, only_frontend_viewable: true)
    self_orders = orders.incomplete
    self_orders = self_orders.where(frontend_viewable: true) if only_frontend_viewable
    self_orders = self_orders.where(store: store) if store
    self_orders = self_orders.where('updated_at > ?', Spree::Config.completable_order_updated_cutoff_days.days.ago) if Spree::Config.completable_order_updated_cutoff_days
    self_orders = self_orders.where('created_at > ?', Spree::Config.completable_order_created_cutoff_days.days.ago) if Spree::Config.completable_order_created_cutoff_days
    last_order = self_orders.order(:created_at).last
    last_order unless last_order.try!(:completed?)
  end

  def self.sales_rep
    # TODO It should be admin and superadmin. Do it later.
    admin
  end

  private
  def associate_quote_requests
    own_quote_requests = Spree::QuoteRequest
      .where(guest_email: self.email).where.not(user_id: self.id)
    own_quote_requests.map do |qr|
      qr.user = self
      qr.comments.map{|c| c.user = self }
      qr.save
    end if own_quote_requests.any?
  end
end
