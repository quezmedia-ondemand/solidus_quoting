module Spree
  Order.class_eval do
    # TODO Only Admin Or SuperAdmin Can Be A Sales Rep. Make Sure This Is Set Later...
    belongs_to :sales_rep, class_name: Spree.user_class, foreign_key: 'sales_rep_id'

    singleton_class.send(:alias_method, :old_incomplete, :incomplete)

    # It needs to be overriden to disconsider quotes
    def self.incomplete
      quote_ids = where(is_quote: true).or(where(quote_closed: true, is_quote: false)).pluck(:id)
      where(completed_at: nil).where.not(id: quote_ids)
    end
  end
end
