module Spree
  OrderMerger.class_eval do
    alias_method :old_merge!, :merge!

    def merge!(other_order, user = nil)
      if other_order.is_quote? || other_order.quote_closed?
        true # Quote Cannot Be Merged With Other Incomplete Order.
      else
        old_merge!(other_order, user)
      end
    end
  end
end
