module Spree
  OrderUpdater.class_eval do
    attr_reader :quote
    delegate :quote_details, to: :quote

    alias_attribute :quote, :order

    # Updates the +quote_state+ attribute according to the following logic:
    #
    # requested when quote is sent to user
    # revision  when user wants something to be updated at the quote
    # approved  when user approved the quote
    # ready     when quote can be used as a normal order
    #
    # The +quote_state+ value helps with reporting, etc. since it provides a quick and easy way to locate Quotes needing attention.
    def update_quote_state
      if quote.is_quote?
        if quote_details.needs_revision?
          quote.quote_state = 'revision'
        elsif quote_details.finished?
          quote.quote_state = 'ready'
          quote.is_quote = false # It should be treated as order now
        else
          quote.quote_state = 'requested'
        end
      end
      quote.quote_state
    end
  end
end
