module Spree
  class QuoteComment < ApplicationRecord
    self.table_name = 'spree_qte_comments'

    attr_accessor :by_admin

    belongs_to :commentable, polymorphic: true
    belongs_to :user, class_name: Spree.user_class

    # A comment can have replies which are essentially comments
    has_many :replies, as: :commentable, class_name: Spree::QuoteComment,
      foreign_key: 'commentable_id'

    validates :body, :commenter_role, presence: true

    scope :made_by_admin, ->(){ where(commenter_role: 'Admin') }

    def commenter
      user.present?? user.email : guest_email
    end
  end
end
