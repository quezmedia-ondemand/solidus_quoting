module Spree
  class Quote < Order
    # Callbacks
    after_initialize :set_initial_config

    # Associations
    has_one :quote_details, class_name: Spree::QuoteDetails, foreign_key: 'spree_order_id', dependent: :destroy
    has_one :quote_request, class_name: Spree::QuoteRequest, foreign_key: 'spree_order_id', dependent: :nullify

    delegate :processing_quote?, :approved_by_user?,
      :needs_revision?, :requested?, :confirmation_delivered?, to: :quote_details, prefix: true

    # Scopes
    default_scope { as_quote }
    scope :as_quote, ->() { where(is_quote: true) }

    # Overriding methods
    def self.model_name
      ActiveModel::Name.new(self, nil, "Quote")
    end

    def quote_details
      super || (self.id?? Spree::QuoteDetails.find_or_create_by(spree_order_id: self.id) : nil)
    end

    # Redefining checkout_flow by adding quote step
    # checkout_flow do
    #   go_to_state :quote, if: ->(quote) { quote.quote_state == "ready" || quote.is_quote? || quote.quote_closed? }
    #   go_to_state :address
    #   go_to_state :delivery
    #   go_to_state :payment, if: ->(order) { order.payment_required? }
    #   go_to_state :confirm
    # end
    #
    # state_machine.before_transition from: :quote, do: :ensure_line_items_present

    def send_quote_to_user
      updater.update_quote_state
      updater.update
      deliver_quote_email
    end

    def finish_quote
      update_column(:quote_closed, true)
      updater.update_quote_state
      updater.update
      deliver_quote_finished_email
    end

    def send_quote_request_notification
      deliver_quote_request_notification
    end

    def approve!
      touch :quote_approved_at
      quote_details.update_column(:approved, true)
      updater.update_quote_state
      updater.update
    end

    def deliver_quote_email
      Spree::QuoteMailer.quote_email(self).deliver_later
      quote_details.update_column(:confirmation_delivered, true)
    end

    def deliver_quote_finished_email
      Spree::QuoteMailer.finish_quote_email(self).deliver_later
      update_column(:quote_closed, true)
    end

    def deliver_quote_request_notification
      Spree::QuoteMailer.quote_request_notification_email(self.quote_request).deliver_later
      Spree::QuoteMailer.admin_quote_request_notification_email(self.quote_request).deliver_later
      quote_details.update_column(:request_notification_delivered, true)
    end

    private
    def set_initial_config
      self.quote_details = Spree::QuoteDetails.find_or_initialize_by(spree_order_id: self.id)
    end
  end
end
