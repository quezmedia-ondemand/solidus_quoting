module Spree
  class QuoteRequest < Spree::Base
    # Callbacks
    before_create :generate_number

    # Associations
    belongs_to :user, class_name: Spree.user_class, foreign_key: 'user_id'
    belongs_to :quote, class_name: Spree::Quote, foreign_key: 'spree_order_id'
    has_many :comments, as: :commentable, class_name: Spree::QuoteComment, foreign_key: 'commentable_id'

    # Cocoon Setup
    accepts_nested_attributes_for :comments, reject_if: :all_blank, allow_destroy: true

    # enums
    enum status: { pending: 0, rejected: 1, approved: 2 }

    # ransack
    self.whitelisted_ransackable_associations = %w[quote]
    self.whitelisted_ransackable_attributes = %w[created_at number status]

    # scopes
    default_scope ->(){ order(created_at: :desc) }

    def deliver_comment_at_quote_request_notification
      Spree::QuoteMailer.quote_request_comment_email(self).deliver_later
      Spree::QuoteMailer.admin_quote_request_comment_email(self).deliver_later
    end

    def deliver_quote_request_notification
      Spree::QuoteMailer.admin_quote_request_notification_email(self).deliver_later
    end

    def check_or_update_owner(owner)
      unless self.user.present?
        self.user = user
        self.comments.map{|c| c.user = user }
        self.save
      end
    end

    private
    def generate_number
      self.number ||= loop do
        random = "QR#{Array.new(9){ rand(9) }.join}"
        break random unless self.class.exists?(number: random)
      end
    end
  end
end
