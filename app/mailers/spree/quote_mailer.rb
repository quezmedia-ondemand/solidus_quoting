module Spree
  class QuoteMailer < BaseMailer

    def quote_email(quote, resend = false)
      @quote = quote
      create_inline_prod_attachments(@quote)
      @store = @quote.store
      subject = build_subject(Spree.t('quote_mailer.quote_email.subject'), resend)

      mail(to: @quote.email, from: from_address(@store), subject: subject)
    end

    def finish_quote_email(quote, resend = false)
      @quote = quote
      create_inline_prod_attachments(@quote)
      @store = @quote.store
      subject = build_subject(Spree.t('quote_mailer.finish_quote_email.subject'), resend)

      mail(to: @quote.email, from: from_address(@store), subject: subject)
    end

    def quote_request_notification_email(quote_request)
      @quote_request = quote_request
      @quote = quote_request.quote
      @store = @quote.try(:store) || Store.first
      subject = "Quote Request Notification ##{@quote_request.number}"

      mail(to: quote_request_receiver(@quote_request), from: from_address(@store), subject: subject)
    end

    def admin_quote_request_notification_email(quote_request)
      @quote_request = quote_request
      @quote = quote_request.quote
      @store = @quote.try(:store) || Store.first
      subject = "Quote Request Notification ##{@quote_request.number} sent to #{quote_request_receiver(@quote_request)}"

      admin_emails = @store.customizations.quote_notification_email.split(/[\s,',]/)
      admin_emails = Spree.user_class.admin if admin_emails.empty?
      mail(to: admin_emails.shift, bcc: admin_emails, from: from_address(@store), subject: subject)
    end

    def quote_request_comment_email(quote_request)
      @quote_request = quote_request
      @quote = quote_request.quote
      @store = @quote.try(:store) || Store.first
      @last_comments = quote_request.comments.made_by_admin.limit(3)
      subject = "Quote Request - New Comment | ##{@quote_request.number}"

      mail(to: quote_request_receiver(quote_request), from: from_address(@store), subject: subject)
    end

    def admin_quote_request_comment_email(quote_request)
      @quote_request = quote_request
      @quote = quote_request.quote
      @store = @quote.try(:store) || Store.first
      @last_comments = quote_request.comments.made_by_admin.limit(3)
      subject = "Quote Request - New Comment | ##{@quote_request.number}"

      admin_emails = @store.customizations.quote_notification_email.split(/[\s,',]/)
      admin_emails = Spree.user_class.admin if admin_emails.empty?
      mail(to: admin_emails.shift, bcc: admin_emails, from: from_address(@store), subject: subject)
    end

    private
    def build_subject(subject_text, resend = false)
      resend_text = (resend ? "[#{Spree.t(:resend).upcase}] " : '')
      "#{resend_text}#{@quote.store.name} #{subject_text} ##{@quote.number}"
    end

    def quote_request_receiver(quote_request)
      if quote_request.guest_email
        quote_request.guest_email
      elsif quote_request.user.present?
        quote_request.user.email
      elsif quote_request.quote.present?
        quote_request.quote.email
      else
        nil
      end
    end

    private
    def create_inline_prod_attachments(quote)
      quote.line_items.map{|li| li.variant.product.images }.flatten.each do |image|
        image_source = Rails.env.development?? ENV['EVERBATT_HOST_URL'] : ''
        attachments.inline["#{image.id}-#{image.attachment_file_name}"] = URI.parse(image_source + image.attachment(:small)).open.read
      end
    end
  end
end
