Deface::Override.new(:virtual_path => "spree/admin/shared/_order_summary",
                     :name => "sales_and_company_tab_summary",
                     :insert_before => "[data-hook='admin_order_tab_subtotal_title']",
                     :partial => "spree/admin/shared/sales_and_company_tab_summary",
                     :disabled => false)
                     
