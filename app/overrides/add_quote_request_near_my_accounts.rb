Deface::Override.new(
  virtual_path: "spree/shared/_nav_bar",
  name: "quote_requests_link",
  insert_top: "ul#nav-bar",
  partial: "spree/shared/quote_requests",
  disabled: false,
  original: 'eb3fa668cd98b6a1c75c36420ef1b238a1fc55ac'
)
