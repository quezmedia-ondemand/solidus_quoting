Deface::Override.new(
  virtual_path: 'spree/users/show',
  name: 'add_quotes_orders_to_account_near_my_orders',
  insert_after: "[data-hook='account_my_orders']",
  partial: 'spree/users/quotes'
)

Deface::Override.new(
  virtual_path: 'spree/users/show',
  name: 'add_quotes_to_account_near_my_orders',
  insert_after: "[data-hook='account_my_orders']",
  partial: 'spree/users/quotes_orders'
)
