Deface::Override.new(:virtual_path => "spree/shared/_link_to_cart",
                     :name => "link_to_cart_at_shared_folder",
                     :replace => "erb[loud]:contains('link_to_cart')",
                     :partial => "spree/shared/cart_link",
                     :original => '5bd6891b81b416b25f842ec5233cf510aafec819',
                     :disabled => false)


Deface::Override.new(:virtual_path => "spree/store/cart_link",
                    :name => "link_to_cart_at_store_folder",
                    :replace => "erb[loud]:contains('link_to_cart')",
                    :partial => "spree/shared/cart_link",
                    :original => '5bd6891b81b416b25f842ec5233cf510aafec819',
                    :disabled => false)
