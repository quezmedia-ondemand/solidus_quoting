class AddQuoteApprovedAtToSpreeOrder < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_orders, :quote_approved_at, :datetime
  end
end
