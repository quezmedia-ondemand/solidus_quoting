class AddQuoteClosedToSpreeOrder < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_orders, :quote_closed, :boolean, default: false
  end
end
