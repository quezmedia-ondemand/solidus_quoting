class AddMailerAttrToSpreeQuoteDetails < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_quote_details, :confirmation_delivered, :boolean, default: false
  end
end
