class RemoveNameFromSpreeQuoteDetails < ActiveRecord::Migration[5.0]
  def change
    remove_column :spree_quote_details, :name, :string
  end
end
