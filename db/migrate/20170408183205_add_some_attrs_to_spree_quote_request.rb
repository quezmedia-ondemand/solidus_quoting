class AddSomeAttrsToSpreeQuoteRequest < ActiveRecord::Migration[5.0]
  def change
    add_reference :spree_quote_requests, :spree_order, foreign_key: true, index: true
    add_column :spree_quote_requests, :guest_email, :string
  end
end
