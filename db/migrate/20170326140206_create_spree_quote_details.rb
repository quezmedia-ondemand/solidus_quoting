class CreateSpreeQuoteDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_quote_details do |t|
      t.string :name
      t.references :spree_order, foreign_key: true, index: true

      t.timestamps
    end
  end
end
