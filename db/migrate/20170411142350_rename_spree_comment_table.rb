class RenameSpreeCommentTable < ActiveRecord::Migration[5.0]
  def change
    rename_table :spree_comments, :spree_qte_comments
  end
end
