class AddReqNotifyFlagToSpreeQuoteDetails < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_quote_details, :request_notification_delivered, :boolean, default: false
  end
end
