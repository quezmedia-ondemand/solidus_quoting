class AddEmailToSpreeQuoteComment < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_qte_comments, :guest_email, :string
  end
end
