class AddStatusToSpreeQuoteRequest < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_quote_requests, :status, :integer, default: 0
  end
end
