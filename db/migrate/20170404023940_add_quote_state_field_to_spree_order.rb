class AddQuoteStateFieldToSpreeOrder < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_orders, :quote_state, :string
  end
end
