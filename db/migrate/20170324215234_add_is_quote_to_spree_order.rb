class AddIsQuoteToSpreeOrder < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_orders, :is_quote, :boolean, default: false
  end
end
