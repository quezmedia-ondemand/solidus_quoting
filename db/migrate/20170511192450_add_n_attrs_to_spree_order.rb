class AddNAttrsToSpreeOrder < ActiveRecord::Migration[5.0]
  def change
    add_reference :spree_orders, :sales_rep, index: true, foreign_key: { to_table: :spree_users }
    add_column :spree_orders, :company, :string
  end
end
