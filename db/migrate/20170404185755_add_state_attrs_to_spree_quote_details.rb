class AddStateAttrsToSpreeQuoteDetails < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_quote_details, :quote_state, :string
    add_column :spree_quote_details, :quote_changed, :boolean, default: false
    add_column :spree_quote_details, :approved, :boolean, default: false
  end
end
