class CreateSpreeComments < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_comments do |t|
      t.text :body
      t.references :commentable, polymorphic: true, index: true
      t.references :user, index: true, foreign_key: { to_table: :spree_users }

      t.timestamps
    end
  end
end
