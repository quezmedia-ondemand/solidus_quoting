class CreateSpreeQuotingOrderAdjustments < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_quoting_order_adjustments do |t|
      t.string :name
      t.string :admin_name

      t.timestamps
    end
  end
end
