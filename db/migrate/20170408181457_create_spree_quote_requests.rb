class CreateSpreeQuoteRequests < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_quote_requests do |t|
      t.references :user, index: true, foreign_key: { to_table: :spree_users }

      t.timestamps
    end
  end
end
