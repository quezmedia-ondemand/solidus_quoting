class AddCommenterRoleToSpreeComment < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_comments, :commenter_role, :string
  end
end
